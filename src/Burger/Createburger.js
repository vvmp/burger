import React from "react";
import BreadLower from "./Burgercomponents/Breadlower";
import BreadUpper from "./Burgercomponents/Breadupper";
import Cheese from "./Burgercomponents/cheese";
import Tomatoes from "./Burgercomponents/tomoto";
import Lettuce from "./Burgercomponents/lettuce";

export default function Createburger(props){
const BurgerStack = props.burgerstack.map((item,index)=>{
    switch (item){
        case 'Cheese':
            return <Cheese key={index}/>;
        case 'Lettuce':
            return <Lettuce key={index}/>;
        case 'Tomatoes':
            return <Tomatoes key={index}/>;
        default:
            return null;
    }
});
return(
    <div>
    <BreadUpper/>
    {BurgerStack}
    <BreadLower/>
    </div>
);
}